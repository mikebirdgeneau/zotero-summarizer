import os
import logging
import pprint
import re
import string
import unicodedata
from heapq import nlargest
from html.parser import HTMLParser
from io import StringIO

import bibtexparser
import contextualSpellCheck
import spacy
from dotenv import load_dotenv
from pyzotero import zotero
from spacy.lang.en.stop_words import STOP_WORDS
from tqdm import tqdm

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

load_dotenv()

zot = zotero.Zotero(os.getenv("ZOTERO_USER"), 'user', os.getenv("ZOTERO_APIKEY"))
pp = pprint.PrettyPrinter(indent=2)


class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.text = StringIO()

    def handle_data(self, d):
        self.text.write(d)

    def get_data(self):
        return self.text.getvalue()


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


def lowercase(text):
    return text.lower()


def clean_linebreak_hyphens(text):
    return text.replace("- ", "")


def remove_accented_chars(text):
    new_text = unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode('utf-8', 'ignore')
    return new_text


def remove_special_characters(text):
    # define the pattern to keep
    pat = r'[^a-zA-z0-9.,!?/:;\'\s]'
    return re.sub(pat, '', text)


def remove_punctuation(text):
    remove = string.punctuation
    remove = remove.replace("-", "").replace(".", "")  # don't remove hyphens/periods.
    pat = r"[{}]".format(remove)  # create the pattern
    return re.sub(pat, '', text)


def remove_numbers(text):
    # define the pattern to keep
    pattern = r'[^a-zA-z.,!?/:;\"\'\s]'
    return re.sub(pattern, '', text)


def remove_whitespace(text):
    return ' '.join(text.split())


def summarizer(document, n=7):
    nlp = spacy.load('en_core_web_sm')
    contextualSpellCheck.add_to_pipe(nlp)
    # tr = pytextrank.TextRank()
    # nlp.add_pipe(tr.PipelineComponent, name='textrank', last=True)
    doc = nlp(document)
    mytokens = [token.text for token in doc]
    # Word Frequency
    word_frequencies = {}
    for word in doc:
        if word.text not in stopwords:
            if word.text not in word_frequencies.keys():
                word_frequencies[word.text] = 1
            else:
                word_frequencies[word.text] += 1
    # Max Word Frequency
    maximum_frequency = max(word_frequencies.values())
    # Relative Frequency:
    for word in word_frequencies.keys():
        word_frequencies[word] = (word_frequencies[word] / maximum_frequency)

    # Sentence Tokens
    sentence_list = [sentence for sentence in doc.sents]

    # Sentence Score via comparing each word with sentence
    sentence_scores = {}
    for sent in sentence_list:
        for word in sent:
            if word.text.lower() in word_frequencies.keys():
                if len(sent.text.split(' ')) < 30:
                    if sent not in sentence_scores.keys():
                        sentence_scores[sent] = word_frequencies[word.text.lower()]
                    else:
                        sentence_scores[sent] += word_frequencies[word.text.lower()]

    # Get Top Sentences:
    summarized_sentences = nlargest(n, sentence_scores, key=sentence_scores.get)
    final_sentences = [w.text.capitalize() for w in summarized_sentences]
    summary = ' '.join(final_sentences)
    return summary


def get_obj_text(key):
    txt = zot.item(key)['data']['note'].replace('\n', ' ').replace('\r', '')
    txt = lowercase(txt)
    txt = strip_tags(txt)
    txt = clean_linebreak_hyphens(txt)
    txt = remove_numbers(txt)
    txt = remove_punctuation(txt)
    txt = remove_accented_chars(txt)
    txt = remove_special_characters(txt)
    txt = remove_whitespace(txt)
    return txt


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    items = zot.items(limit=1000, itemType="note")
    keys = [item['key'] for item in items]
    stopwords = list(STOP_WORDS)

    # Sample summarization for a single document:
    logger.info("Summarizing Documents...")

    with open("summaries.md", "w") as f:
        f.write("# Literature Review: Abstract and Summary")

        # Loop over & Summarize Articles; save to Zotero
        for key in tqdm(keys):
            # Fetch Objects & Summarize OCR Text
            parent = zot.item(zot.item(key)['data']['parentItem'])
            txt = get_obj_text(key)
            parent['data']['extra'] = summarizer(txt, 7)
            parent['data']['title'] = parent['data']['title'].title()

            # Write output to Markdown with Abstract & Summary
            logger.info('* Processing: '+remove_whitespace(parent['data']['title']))
            f.write("## " + remove_whitespace(parent['data']['title']) + "\n")
            f.write("### Abstract\n")
            f.write(remove_whitespace(parent['data']['abstractNote']) + "\n")
            f.write("### Summary\n")
            f.write(remove_whitespace(parent['data']['extra']) + "\n\n")

            # Update extra text in Zotero
            try:
                zot.update_item(parent)
            except Exception as e:
                logger.error(e)

    # Write *.bib file for LaTeX bibliography
    logger.info("Generating *.bib File...")

    zot.add_parameters(content='bibtex', style='apa')
    bib = zot.top()
    bibtexparser.dump(bib, "references.bib")

    logger.info("Done.")
