
import os
import logging
import pprint
import re
import string
import unicodedata
from heapq import nlargest
from html.parser import HTMLParser
from io import StringIO

import bibtexparser
import contextualSpellCheck
import spacy
from dotenv import load_dotenv
from pyzotero import zotero
from spacy.lang.en.stop_words import STOP_WORDS
from tqdm import tqdm

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

load_dotenv()

zot = zotero.Zotero(os.getenv("ZOTERO_USER"), 'user', os.getenv("ZOTERO_APIKEY"))
pp = pprint.PrettyPrinter(indent=2)

if __name__ == '__main__':

    zot.add_parameters(content='bibtex', style='apa')
    bib = zot.top()
    with open('references.bib','w') as f:
        for bib_entry in tqdm(bib):
            f.write(bib_entry)
